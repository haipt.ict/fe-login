let loginFailed = localStorage.getItem("loginFailed");

if (loginFailed != undefined && loginFailed != null) {
    loginFailed = Number(loginFailed);
    $('#login-status').html("Login Failed " + loginFailed + "/5");
}

let countDownValue = localStorage.getItem("countDownValue");

if (countDownValue != undefined && countDownValue != null) {
    countDown(false);
}

$("#login-button").click(function(e) {
    e.preventDefault();
    let loginUrl = "http://139.180.138.171:8080/api/authenticate";

    let username = $("#username").val();
    let password = $("#password").val();

    if (username == "" || password == "") {
        alert("Please input username, password!");
        return;
    }

    let loginData = {
        "username": username,
        "password": password
    }

    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "POST",
        url: loginUrl,
        data: JSON.stringify(loginData),
        success: function(data) {
            window.location.href  = "welcome";
            resetLocalSg();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            countDown(true);
        }
    });

});


function countDown(isError) {

    let loginFailed = localStorage.getItem("loginFailed");
    let countDownValue = localStorage.getItem("countDownValue");

    if (loginFailed == undefined || loginFailed == null) {
        loginFailed = 0;
    } else {
        loginFailed = Number(loginFailed);
    }

    if (isError && loginFailed > 4) {
        loginFailed = 5;
    } else if (isError) {
        loginFailed = loginFailed + 1;
    }

    localStorage.setItem("loginFailed", loginFailed);

    $('#login-status').html("Login Failed " + loginFailed + "/5");

    if (loginFailed < 5) {
        return;
    }

    $('#respModal').modal({backdrop: 'static', keyboard: false});

    $("#res-message").html("Login Failed "  + loginFailed + "/5. Please wait...");

    let timer2 = "5:00";

    if (countDownValue != undefined && countDownValue != null && Number(countDownValue) > 0) {
        countDownValue = Number(countDownValue);
        let tempMinutes = Math.floor(countDownValue/60);
        let tempSeconds = countDownValue - 60 * tempMinutes;

        if (tempSeconds < 10) {
            tempSeconds = "0" + String(tempSeconds);
        }
        timer2 = String(tempMinutes) + ":" + String(tempSeconds);
    }

    $("#count-down-message").html(timer2);
    let interval = setInterval(function() {

        let timer = timer2.split(':');
        let minutes = parseInt(timer[0], 10);
        let seconds = parseInt(timer[1], 10);
        --seconds;
        minutes = (seconds < 0) ? --minutes : minutes;
        if (minutes < 0) clearInterval(interval);
        seconds = (seconds < 0) ? 59 : seconds;
        seconds = (seconds < 10) ? '0' + seconds : seconds;
        timer2 = minutes + ':' + seconds;
        $("#count-down-message").html(timer2);

        let temp = 60 * Number(minutes) + Number(seconds);

        if (temp > 0) {
            localStorage.setItem("countDownValue", 60 * Number(minutes) + Number(seconds));
        }

        if (minutes == 0 && seconds == 0) {
            $('#respModal').modal('toggle');
            $('#login-status').html("Login Failed 0/5");
            resetLocalSg();
        }
    }, 1000);

};

function resetLocalSg() {
    localStorage.setItem("loginFailed", "0");
    localStorage.removeItem("countDownValue");
}