const express = require('express');
const path = require('path');

const app = express();
const port = process.env.PORT || 9000;

// sendFile will go here
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/style.css', function(req, res) {
  res.sendFile(path.join(__dirname, '/style.css'));
});

app.get('/login.js', function(req, res) {
  res.sendFile(path.join(__dirname, '/login.js'));
});

app.get('/welcome', function(req, res) {
  res.sendFile(path.join(__dirname, '/welcome.html'));
});


app.listen(port);
console.log('Server started at http://localhost:' + port);